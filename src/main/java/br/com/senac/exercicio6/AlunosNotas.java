package br.com.senac.exercicio6;

import java.util.ArrayList;
import java.util.List;

public class AlunosNotas {

    public static void main(String[] args) {

        Aluno messi = new Aluno("Messi", 10);
        Aluno cr7 = new Aluno("CR7", 5);
        Aluno neymar = new Aluno("Neymar", 2);

        List<Aluno> lista = new ArrayList<>();

        lista.add(messi);
        lista.add(cr7);
        lista.add(neymar);

        Aluno alunoAprovado = boletimAprovado(lista);
        Aluno alunoRecuperacao = boletimRecuperacao(lista);
        Aluno alunoReprovado = boletimReprovado(lista);
    }

    public static Aluno boletimAprovado(List<Aluno> lista) {

        Aluno alunoAprovado = lista.get(0);

        for (Aluno a : lista) {
            if (a.getNota() >= 7) {
                alunoAprovado = a;
                System.out.println("Aluno aprovado : "  + alunoAprovado.getNome());
            }
        }

        return alunoAprovado;
    }

    public static Aluno boletimRecuperacao(List<Aluno> lista) {

        Aluno alunoRecuperacao = lista.get(0);

        for (Aluno a : lista) {
            if (a.getNota() >= 4 && a.getNota() <= 6) {
                alunoRecuperacao = a;
                System.err.println("Aluno de recuperação: " + alunoRecuperacao.getNome());
            }
        }

        return alunoRecuperacao;

    }

    public static Aluno boletimReprovado(List<Aluno> lista) {

        Aluno alunoReprovado = lista.get(0);

        for (Aluno a : lista) {
            if (a.getNota() <= 2) {
                alunoReprovado = a;
                System.err.println("Aluno de reprovado: " + alunoReprovado.getNome());
            }
        }
        return alunoReprovado;
    }

}
