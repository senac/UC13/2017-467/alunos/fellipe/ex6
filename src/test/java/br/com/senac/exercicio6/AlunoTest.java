package br.com.senac.exercicio6;

import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

public class AlunoTest {

    public AlunoTest() {
    }

    @Test
    public void messiAprovado() {

        Aluno messi = new Aluno("Messi", 7);
        Aluno cr7 = new Aluno("CR7", 5);
        Aluno neymar = new Aluno("Neymar", 2);
        List<Aluno> lista = new ArrayList<>();

        lista.add(messi);
        lista.add(cr7);
        lista.add(neymar);

        Aluno alunoAprovado = AlunosNotas.boletimAprovado(lista);

        assertEquals(messi, alunoAprovado);

    }

    @Test
    public void cr7Recuperacao() {

        Aluno messi = new Aluno("Messi", 7);
        Aluno cr7 = new Aluno("CR7", 5);
        Aluno neymar = new Aluno("Neymar", 2);
        List<Aluno> lista = new ArrayList<>();

        lista.add(messi);
        lista.add(cr7);
        lista.add(neymar);

        Aluno alunoRecuperacao = AlunosNotas.boletimRecuperacao(lista);

        assertEquals(cr7, alunoRecuperacao);

    }

    @Test
    public void neymarReprovado() {

        Aluno messi = new Aluno("Messi", 7);
        Aluno cr7 = new Aluno("CR7", 5);
        Aluno neymar = new Aluno("Neymar", 2);
        List<Aluno> lista = new ArrayList<>();

        lista.add(messi);
        lista.add(cr7);
        lista.add(neymar);

        Aluno alunoReprovado = AlunosNotas.boletimReprovado(lista);

        assertEquals(neymar, alunoReprovado);

    }

}
